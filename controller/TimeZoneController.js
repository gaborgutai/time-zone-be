const timeZoneService = require('/time-zone-app/service/TimeZoneService');
const authService = require('/time-zone-app/service/AuthService');
var bodyParser = require('body-parser');
const jwt = require("jsonwebtoken");

module.exports = (app) => {
  app.use((req, res, next) => {
    if (req.originalUrl === '/login') {
      next();
    } else {
      const authHeader = req.headers['authorization'];
      const token = authHeader && authHeader.split(' ')[1]
      if (token == null) return res.sendStatus(401);
    
      jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
        if (err) return res.sendStatus(403);
        req.user = user;
        next();
      });
    }
  });

  app.get('/server-time', (req, res) => {
    res.send(timeZoneService.getTime());
  });

  app.get('/server-time-zones', (req, res) => {
    res.send({timeZones: [{name: 'Budapest', code: 'Europe/Budapest'}, {name: 'Palau', code: 'Pacific/Palau'}]});
  });
  
  app.post('/login', bodyParser.json() ,(req, res) => {
    const token = authService.generateAccessToken({ username: req.body.username });
    res.send(token);
  });
}

