const express = require('express');
const mysql = require('mysql2');
const { Sequelize, DataTypes } = require('sequelize');
const dotenv = require("dotenv");
const cors = require('cors');

const app = express();
app.use(cors());
dotenv.config();

const timeZoneService = require('/time-zone-app/service/TimeZoneService');
const authService = require('/time-zone-app/service/AuthService');
var bodyParser = require('body-parser');
const jwt = require("jsonwebtoken");


const port = 3001;
const connection = mysql.createConnection({
  host: 'localhost',
  port: '3306',
  user: 'root',
  password: 'Q1w2e3r4',
  database: 'time-zone-app'
});

connection.connect();

const sequelize = new Sequelize('time-zone-app', 'root', 'Q1w2e3r4', {
  host: 'localhost',
  port: '3306',
  dialect: 'mysql'
});

let User;
let TimeZone;

const sequelizeInit = async () => {
  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
    User = await require('/time-zone-app/persistance/Entities').User(sequelize, DataTypes);
    TimeZone = await require('/time-zone-app/persistance/Entities').TimeZone(sequelize, DataTypes);
    await sequelize.sync();
    if ((await User.findAll()).length === 0) {
      await User.create({name: 'Teszt Elek', userName: 'tesztelek', password: 'AQSWDEfr12', });
      await User.create({name: 'Teszt Ilona', userName: 'tesztilona', password: 'AQSWDEfr13', });
    }
    if ((await TimeZone.findAll()).length === 0) {
      await TimeZone.create({name: 'Budapest', code: 'Europe/Budapest'});
      await TimeZone.create({name: 'Noronha', code: 'America/Noronha'});
      await TimeZone.create({name: 'Atikokan', code: 'America/Atikokan'});
      await TimeZone.create({name: 'Ulaanbaatar', code: 'Asia/Ulaanbaatar'});
      await TimeZone.create({name: 'Vientiane', code: 'Asia/Vientiane'});
      await TimeZone.create({name: 'Zurich', code: 'Europe/Zurich'});
    }
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}

sequelizeInit();

app.use((req, res, next) => {
  if (req.originalUrl === '/login') {
    next();
  } else {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401);
  
    jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
      if (err) return res.sendStatus(403);
      req.user = user;
      next();
    });
  }
});

app.get('/server-time', (req, res) => {
  res.send(timeZoneService.getTime());
});

findTimeZones = async () => {
  const timeZones = await TimeZone.findAll();
  return timeZones;
}

app.get('/server-time-zones', async (req, res) => {
  const timeZones = await findTimeZones();
  console.log("All users:", JSON.stringify(timeZones, null, 2));
  res.send({timeZones: timeZones});
});

app.post('/login', bodyParser.json() ,(req, res) => {
  const token = authService.generateAccessToken({ username: req.body.username });
  res.send(token);
});

app.listen(port, () => console.log(`Time Zone App listening at http://localhost:${port}`));