module.exports = {
  User: (sequelize, DataTypes) => sequelize.define('user', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    userName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {}),

  TimeZone: (sequelize, DataTypes) => sequelize.define('time-zone', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    code: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {})
}