const jwt = require("jsonwebtoken");

module.exports = {
  generateAccessToken: (userName) => {
    return jwt.sign(userName, process.env.TOKEN_SECRET, { expiresIn: '3600s' });
  }
}
