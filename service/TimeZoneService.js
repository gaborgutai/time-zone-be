const moment = require('moment-timezone');

module.exports = {
  getTime: () => {
    return moment().format();
  },
}